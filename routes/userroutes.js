const express = require('express')
var router = express()
const userController = require('../controllers/user.controller')
const bodyparser = require('body-parser');

router.use(bodyparser.json())
router.get('/:userId', userController.getById);
router.post('/create', userController.create);
router.post('/authenticate', userController.authenticate);

module.exports = router