const AppConstants = require('./appConstants.js');
const CustomerStore = require('./customerStore.js');

class MessageStore {
    constructor() {
        // All save all the messages
        this.messages = {};
    }

    _createMessageStore(customerId) {
        this.messages[customerId] = [];
        console.log("Message Store Created for Candidate : " + customerId);
    }

    _pushMessage(message) {
        this.messages[message.customerId].push(message);
    }

    _getAllMessages(customerId) {
        return Promise.resolve(this.messages[customerId]);
    }
}

module.exports = MessageStore;