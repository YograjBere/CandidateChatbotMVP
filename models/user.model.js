const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
    id: String,
    email: String,
    password: String,
    firstName: String,
    lastName: String,
    role: String,
    token: String
});

module.exports = mongoose.model("User", userSchema);