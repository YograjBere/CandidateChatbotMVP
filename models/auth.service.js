const bcrypt = require("bcrypt");
const config = require("config");
const jwt = require("jsonwebtoken");

JwtToken = { sub: "" };

function AuthService() {
    this.hashPassword = async function(password, salt = 10) {
        return await bcrypt.hash(password, salt);
    };

    this.comparePasswords = async function(password, hashedPassword) {
        return await bcrypt.compare(password, hashedPassword);
    };

    this.generateToken = function(payload) {
        return jwt.sign({ sub: payload }, config.get("App.auth.key"), {
            expiresIn: config.get("App.auth.tokenExpiresIn"),
        });
    };

    this.decodeToken = function() {
        return jwt.verify(token, config.get("App.auth.key"));
    };
}

module.exports = new AuthService()