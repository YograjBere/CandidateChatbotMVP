module.exports = {
    EVENT_CUSTOMER_CONNECTED: 'customer_connected',
    EVENT_CUSTOMER_DISCONNECTED: 'customer_disconnected',
    EVENT_CUSTOMER_MESSAGE: 'customer_message',
    EVENT_OPERATOR_MESSAGE: 'operator_message',
    EVENT_SYSTEM_ERROR: 'system error',
    EVENT_DISCONNECT: 'disconnect',
    CONTEXT_OPERATOR_REQUEST: 'operator_request',
    INTENT_OPERATOR_REQUEST: 'switch_to_human_agent',
    OPERATOR_GREETING: `Hello, I'm a human. How can I help you?`,
    EVENT_GET_CUSTOMERS: "get_customers",
    EVENT_OPERATOR_JOINED_CUSTOMER: "operator_joined_customer",
    EVENT_CUSTOMER_REQUESTED_OPERATOR: "customer_requested_operator",
    EVENT_OPERATOR_REQUESTED_JOIN: "operator_requested_join",
    EVENT_PEEK_CUSTOMER_MESSAGE: "peek_customer_messages",
    TALK_TO_HUMAN_OPTION: "Talk to human operator ?"
};