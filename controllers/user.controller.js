const userModel = require("../models/user.model");
const mongoose = require("mongoose");
const authService = require("../models/auth.service");
var userController = {};

// get user by Id
function getById(req, res, next) {
    let userId = req.params.userId;
    userModel.findById({ id: userId }).then((user) => res.send(user));
}

// create user into database
async function create(req, res, next) {
    let email = req.body.email;
    let password = await authService.hashPassword(req.body.password);
    let firstName = req.body.firstname;
    let lastName = req.body.lastname;
    let role = req.body.role;
    let newUser = new userModel({
        email,
        password,
        firstName,
        lastName,
        role
    });


    newUser.save().then((data) => {
        res.status(200).send(data);
    });
}

// authenticate user
function authenticate(req, res, next) {
    let userId = req.body.email;
    let password = req.body.password;

    if (userId === "") res.status(400).send(`user name is empty: ${userId}`);

    userModel.findOne({ email: userId }).then((user) => {
        if (!user) {
            res.status(404).send(`user not found with Id: ${userId}`);
            return;
        }


        authService.comparePasswords(password, user.password).then((isMatch) => {
            if (isMatch) {
                var generatedToken = authService.generateToken({
                    id: user.id,
                    role: user.role,
                });

                user.token = generatedToken;
                delete user.password;
                res.status(200).send(user);
            } else {
                res.status(401).send("Incorrect user or password..");
            }
        });
    });
}

function deleteUser(req, res, next) {}

userController.create = create;
userController.getById = getById;
userController.authenticate = authenticate;

module.exports.create = create;
module.exports.getById = getById;
module.exports.authenticate = authenticate;