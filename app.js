// Load third party dependencies
const express = require('express');
const mongoose = require('mongoose');
var cors = require('cors');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"],
        credentials: false
    }
});
const config = require('config');
app.options('*', cors());
app.use(cors());
app.use(bodyParser.json());

console.log(config.get('App.database.mongoUrl'));

// Load our custom classes
const CustomerStore = require('./customerStore.js');
const MessageStore = require('./messageStore.js');
const MessageRouter = require('./messageRouter.js');
const userRoutes = require('./routes/userroutes')

const port = process.env.PORT || config.get('App.port');

// controller routes
app.use('/users', userRoutes);

// Grab the service account credentials path from an environment variable
const keyPath = path.join(config.util.getEnv('NODE_CONFIG_DIR'), config.get("App.dialogflow.DF_SERVICE_ACCOUNT_PATH"))
    // const keyPath = config.get("App.dialogflow.DF_SERVICE_ACCOUNT_PATH");
if (!keyPath) {
    console.log('You need to specify a path to a service account keypair in environment variable DF_SERVICE_ACCOUNT_PATH. See README.md for details.');
    process.exit(1);
}

// Load and instantiate the Dialogflow client library
const { SessionsClient } = require('dialogflow');
const dialogflowClient = new SessionsClient({
    keyFilename: keyPath
});

// Grab the Dialogflow project ID from an environment variable
const projectId = config.get("App.dialogflow.DF_PROJECT_ID");
if (!projectId) {
    console.log('You need to specify a project ID in the environment variable DF_PROJECT_ID. See README.md for details.');
    process.exit(1);
}

// Instantiate our app
const customerStore = new CustomerStore();
const messageStore = new MessageStore();
const messageRouter = new MessageRouter({
    customerStore: customerStore,
    messageStore: messageStore,
    dialogflowClient: dialogflowClient,
    projectId: projectId,
    customerRoom: io.of('/customer'),
    operatorRoom: io.of('/operator'),
    app
});

// Serve static html files for the customer and operator clients
app.use(express.static('static/dist/chatbot-ui-app'));

// Begin responding to websocket and http requests
messageRouter.handleConnections();

mongoose
    .connect(config.get('App.database.mongoUrl'), { useNewUrlParser: true })
    .then(() => {
        http.listen(port, () => {
            console.log(`server listening on *:${port}`);
        });
    });