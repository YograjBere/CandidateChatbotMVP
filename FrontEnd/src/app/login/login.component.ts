import { HttpStatusCode } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../shared/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../shared/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getRouteByRole } from '../shared/roleauth.guard';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = <FormGroup>{};
  @ViewChild('userEmail') email: any;
  @ViewChild('userPassord') password: any;
  returnUrl: string = '';
  error = '';
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate([`/${this.returnUrl}`]);
    }
  }

  get f() { return this.loginForm.controls; }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      useremail: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnUrl = getRouteByRole(this.authenticationService.currentUserValue.role);
  }

  onSubmit() {
    let userEmail = this.f.useremail.value;
    let password = this.f.password.value;

    this.authenticationService.login(userEmail, password)
      .pipe(first())
      .subscribe(
        data => {
          this.returnUrl = getRouteByRole(this.authenticationService.currentUserValue.role);
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}
