import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from '@angular/common/http';
import { User } from "../models/user.model";

@Injectable()
export class UserService {
  private userServiceUrl: string = environment.apiBaseUrl
  public currentUserValue: User = <User>{};
  private apiBaseUrl: string = `${this.userServiceUrl}/users`;
  constructor(private httpClient: HttpClient) { }

  getUserById(userId: string) {
    return this.httpClient.get(`${this.apiBaseUrl}/${userId}`);
  }

  authenticate(userId: string, password: string) {

    return this.httpClient.post(`${this.apiBaseUrl}/authenticate`, { email: userId, password });
  }
}
