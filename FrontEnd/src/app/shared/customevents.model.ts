export class CustomEvents{
  static CUSTOMER_MESSAGE = "customer_message";
  static OPERATOR_MESSAGE = "operator_message";
  static GET_CUSTOMERS = "get_customers";
  static OPERATOR_JOINED_CUSTOMER = "operator_joined_customer";
  static CUSTOMER_REQUESTED_OPERATOR = "customer_requested_operator";
  static OPERATOR_REQUESTED_JOIN = "operator_requested_join";
  static PEEK_CUSTOMER_MESSAGE = "peek_customer_messages";
}
