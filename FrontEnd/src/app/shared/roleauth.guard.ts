import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRole } from '../models/user.model';
import { AuthenticationService } from './auth.service';

export var authRoutes =
{
  operator: "operator",
  customer: ""
}

export function getRouteByRole(routeRole: string) {
  var routeToNavigate = Object.entries(authRoutes)
    .map(([e, t]) => ({ e, t }))
    .find(e => e.e.toUpperCase() === routeRole?.toUpperCase());
  if (!routeToNavigate)
    return 'login';

  return routeToNavigate.t;
}

@Injectable({ providedIn: 'root' })
export class RoleAuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authService.currentUserValue;

    // if authenticated
    if (!currentUser.token || !currentUser.role) {
      this.router.navigate([`/login`], { queryParams: { returnUrl: state.url } });
      return false;
    };

    // authenticated, check role for authorization
    if (route.data.roles && route.data.roles.indexOf(currentUser.role) !== -1) {
      // role authorised so redirect to accessible route
      return true;
    }

    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
