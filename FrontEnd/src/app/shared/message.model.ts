export class Message {
  messageType:string
  content: string;
  from:string;
  customerId:string;
  timestamp: Date;
  intent:string
  constructor(messageType:string, content: string,from: string, customerId:string, timestamp: Date,intent:string){
      this.messageType = messageType;
      this.content = content;
      this.from = from;
      this.customerId = customerId;
      this.timestamp = timestamp;
      this.intent = '';
  }
}
