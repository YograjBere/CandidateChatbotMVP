import { Component, OnInit } from '@angular/core';
import { io } from "socket.io-client";
import { CustomEvents } from '../shared/customevents.model';
import { faLocationArrow, faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { environment } from "../../environments/environment";
import { Menu, MenuStack } from '../models/menustack';
import { ChatMessage, ChatMessageType } from '../models/chatmessage';

@Component({
  selector: 'app-candidate-chat',
  templateUrl: './candidate-chat.component.html',
  styleUrls: ['./candidate-chat.component.css']
})
export class CandidateChatComponent implements OnInit {

  faLocationArrow = faLocationArrow;
  faPaperclip = faPaperclip;
  title = '';
  currentMessage = "";
  socket = io(`${environment.socketUrl}/customer`);
  messages: Array<ChatMessage> = new Array<ChatMessage>();
  menusDisplayed: MenuStack = new MenuStack();
  currentMenu: Menu[] = [];
  isShow = true;
  changeText: boolean = false;
  selectChoiceMessage: string = "Please select below one option!";

  isShowHeading:boolean=false;
  ngOnInit(): void {
    this.socket.on("connect", () => {
      console.log(this.socket.id);
    });

    this.socket.on("connect_error", (e) => {
      console.log(e);
    });

    this.socket.on(CustomEvents.CUSTOMER_MESSAGE, (message: ChatMessage) => {
      if (message.content === '') message.content = this.selectChoiceMessage;
      console.log("Reached " + JSON.stringify(message));
      this.messages.push(message);
    });
  }

  onOptionClick(optionText: string) {
    this.currentMessage = optionText;
    this.onSend();
  }

  onSend() {
    console.log("Send : " + this.currentMessage);
    let message = new ChatMessage({
      messageType: ChatMessageType.Message,
      content: this.currentMessage,
      from: 'User',
      customerId: this.socket.id,
      timestamp: new Date()
    });

    this.messages.push(message);
    this.socket.emit(CustomEvents.CUSTOMER_MESSAGE, message);
    console.log("JSON: " + JSON.stringify(this.messages));
    this.currentMessage = '';
  }

  onKey(event: any) {
    if (event.keyCode == 13) {
      this.onSend();
    }
  }


  tooltip() {
    this.changeText = true;
  }

  toggleDisplay() {
    this.isShow = !this.isShow;
  }

}
