import { ChatMessage } from "./chatmessage";

export class Customer
{
  constructor(public id: string,
    public name: string,
    public isConnected: boolean,
    public isSelected: boolean, public messages: Array<ChatMessage>) {}
}
