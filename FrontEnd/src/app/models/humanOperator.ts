export class HumanOperator {
  constructor(public id: string, public name: string, isConnected: boolean) { }
}
