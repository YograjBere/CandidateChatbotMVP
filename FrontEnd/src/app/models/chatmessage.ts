import { Menu } from "./menustack";

export enum ChatMessageType {
  Message = "message",
  Info = "info",
  RichResponse = "richResponsse"
}

export enum RichResponseType {
  Chips = "chips",
  Carousel = "carousel",
  Image = "image"
}

export class ChatMessage {
  public messageType: ChatMessageType =  ChatMessageType.Message;
  public content: string = '';
  public from: string = '';
  public timestamp: Date=  new Date();
  public intent: string = '';
  public customerId?: string;
  public richResponseType?: RichResponseType
  public options?: string[];


  constructor(options?: Partial<ChatMessage>) {
    Object.assign(this, options);
  }

  public get Menus() : Menu[] | undefined {
    return this.options?.map(e => new Menu(e, this.intent));
  }
}

// export class TextResponseMessage extends ChatMessage {
//   constructor(
//     public content: string,
//     public from: string,
//     public time: Date,
//     public intent: string,
//     public customerId?: string) {
//     super(ChatMessageType.Message, content, from, time, intent, customerId);
//   }
// }

// export class RichResponseMessage extends ChatMessage {
//   constructor(public type: ChatMessageType,
//     public content: string,
//     public from: string,
//     public time: Date,
//     public intent: string,
//     public customerId?: string,
//     public richResponseType?: RichResponseType) {
//     super(type, content, from, time, intent, customerId);
//   }
// }

// export class RichChipsResponseMessage extends RichResponseMessage {
//   public menus: Menu[];

//   constructor(
//     public content: string,
//     public from: string,
//     public time: Date,
//     public intent: string,
//     public options: string[],
//     public customerId?: string,
//   ) {
//     super(ChatMessageType.RichResponse, content, from, time, intent, customerId, RichResponseType.Chips);
//     this.menus = options?.map((e: string) => new Menu(e, e)) || [];
//   }
// }

// export class RichResponseFactory {
//   public static create(
//     content: string,
//     from: string,
//     time: Date,
//     intent: string,
//     customerId?: string,
//     richResponseType?: RichResponseType,
//     options?: string[] | undefined) {
//     switch (richResponseType) {
//       case RichResponseType.Chips:
//         return new RichChipsResponseMessage(content, from, time, intent, (options || []), customerId);
//       default:
//         throw new Error("Invalid or unimplement rich response type");
//     }
//   }
// }
