export class User {
  constructor(
    public email: string,
    public firstName: string,
    public lastName: string,
    public role: string,
    public password?: string,
    public token?: string) { }

  public get Name(): string {
    return this.firstName + " " + this.lastName;
  }
}

export enum UserRole
{
  operator = "Operator",
  customer = "Customer"
}
