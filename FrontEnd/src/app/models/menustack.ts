export class Menu {
  constructor(public text: string, public intent: string) { }
}

interface IStack<T> {
  push(item: T): void;
  pop(): T | undefined;
  peek(): T | undefined;
  size(): number;
}

export class Stack<T> implements IStack<T>
{
  private top: number = -1
  private storage: T[] = [];

  push(item: T): void {
    this.storage.push(item);
    this.top++;
  }

  pop(): T | undefined {
    this.checkTop();

    var value = this.storage.pop();
    this.top--;
    return value;
  }

  peek(): T | undefined {
    this.checkTop();
    return this.storage[this.top];
  }

  checkTop() {
    if (this.top <= -1)
      return;
  }

  size(): number {
    return this.storage.length;
  }
}

export class MenuStack extends Stack<Menu[]> {
}
