import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../shared/auth.service';
import { environment } from "../../environments/environment";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input()
  showHeading: boolean = true; 

  constructor(private authenticationService : AuthenticationService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
  }

  signOut() {
    this.authenticationService.logout();
    this.router.navigate([environment.loginUrl]);
  }


}
