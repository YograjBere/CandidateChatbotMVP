import { Component, OnInit } from '@angular/core';
import { io } from "socket.io-client";
import {Message} from '../shared/message.model';
import {CustomEvents} from '../shared/customevents.model';
import { environment } from "../../environments/environment";
import { User } from '../models/user.model';
import { AuthenticationService } from '../shared/auth.service';

@Component({
  selector: 'app-recruiter-chat',
  templateUrl: './recruiter-chat.component.html',
  styleUrls: ['./recruiter-chat.component.css']
})
export class RecruiterChatComponent implements OnInit {
  selectedIndex: number = 0;
  setIndex(index: number) {
     this.selectedIndex = index;
  }
  socket = io(`${environment.socketUrl}/operator`);
  all_customers:string[] = [];
  joined_customers:string[] = [];
  combined_customers:string[]=[];
  messages: Message[] = [];
  customerId:string;
  currentMessage:string;
  operatorId:string;

  constructor(private authService: AuthenticationService) {
    this.customerId = '';
    this.currentMessage = '';
    this.operatorId = '';
   }

  ngOnInit(): void {

    this.socket.on("connect", () => {
      this.operatorId = this.socket.id;
      console.log("Operator Id: "+this.operatorId);
      this.socket.emit(CustomEvents.GET_CUSTOMERS,this.operatorId, (customers:string)=>{
        let customersObj = JSON.parse(customers);
        this.all_customers = customersObj.all_customers;
        this.joined_customers = customersObj.joined_customers;
      });
    });



    this.socket.on(CustomEvents.CUSTOMER_MESSAGE, (message) => {
      if(message.customerId === this.customerId){
        this.messages.push(message);
      }
    });

    this.socket.on(CustomEvents.OPERATOR_JOINED_CUSTOMER, (customerId) => {
      if(!this.joined_customers.includes(customerId)){
        if(this.all_customers.includes(customerId)){
          let index = this.all_customers.indexOf(customerId);
          this.all_customers.splice(index, 1);
          if(this.customerId == customerId){
            this.messages = [];
            this.customerId = '';
          }
        }
      }
    });

    this.socket.on(CustomEvents.CUSTOMER_REQUESTED_OPERATOR,(customerId:string)=>{
      this.all_customers.push(customerId);
    });

  }

  onRowClicked(customerSelected:any){
    this.customerId = customerSelected;
    this.socket.emit(CustomEvents.PEEK_CUSTOMER_MESSAGE,this.customerId,(messages:Message[])=>{
      this.messages = messages;
    });
  }

  onSend(textInput:any) {
    this.currentMessage = textInput.value;
    console.log("Send : "+this.currentMessage);
    textInput.value = '';
    var message = new Message("message",this.currentMessage,'Operator',this.customerId,new Date(),"");
    this.messages.push(message);
    this.socket.emit(CustomEvents.OPERATOR_MESSAGE,message);
  }

  onJoin() {
    debugger;
    this.joined_customers.push(this.customerId);
    var recruiterName = this.authService.currentUserValue.firstName;
    var message = new Message("info",`Operator ${recruiterName} joined the conversation`,'info',this.customerId,new Date(),"");
    this.messages.push(message);
    this.socket.emit(CustomEvents.OPERATOR_REQUESTED_JOIN,{customerId:this.customerId, operatorId:this.operatorId, message:message});
  }

  HasOperatorJoined(){
    if(this.joined_customers.includes(this.customerId)){
      return true;
    }
    else{
      return false;
    }
  }
}
