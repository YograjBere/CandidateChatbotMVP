import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandidateChatComponent } from './candidate-chat/candidate-chat.component';
import { LoginComponent } from './login/login.component';
import { UserRole } from './models/user.model';
import { RecruiterChatComponent } from './recruiter-chat/recruiter-chat.component';
import { RoleAuthGuard } from './shared/roleauth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: CandidateChatComponent, canActivate: [RoleAuthGuard], data: { roles: [UserRole.customer] } },
  { path: 'operator', component: RecruiterChatComponent, canActivate: [RoleAuthGuard], data: { roles: [UserRole.operator] } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
