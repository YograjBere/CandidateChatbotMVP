export class AppConstants {
  static CUSTOMER_MESSAGE_EVENT = "customer message";
  static SENT_MESSAGE = "s";
  static RECEIVED_MESSAGE = "r";
  static CUSTOMER_CONNECTED_EVENT="customer connected";
  static CUSTOMER_DISCONNECTED_EVENT="customer connected";
  static SYSTEM_ERROR_EVENT="connect_error";
  static CUSTOMER_DEFAULT_NAME="Candidate";
}
