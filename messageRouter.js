const AppConstants = require('./appConstants.js');
const CustomerStore = require('./customerStore.js');
const MessageStore = require('./messageStore.js');
const CustomerConnectionHandler = require('./customerConnectionHandler.js');
const OperatorConnectionHandler = require('./operatorConnectionHandler.js');

// Routes messages between connected customers, operators and Dialogflow agent
class MessageRouter {
    constructor({
        customerStore,
        messageStore,
        dialogflowClient,
        projectId,
        customerRoom,
        operatorRoom,
        app
    }) {
        // Dialogflow client instance
        this.client = dialogflowClient;
        // Dialogflow project id
        this.projectId = projectId;
        // An object that handles customer data persistence
        this.customerStore = customerStore;
        this.messageStore = messageStore;
        // Socket.io rooms for customers and operators
        this.customerRoom = customerRoom;
        this.operatorRoom = operatorRoom;
        // All active connections to customers or operators
        this.customerConnections = {};
        this.operatorConnections = {};
        this.app = app;
    }

    // Attach event handlers and begin handling connections
    handleConnections() {
        this.customerRoom.on('connection', this._handleCustomerConnection.bind(this));
        this.operatorRoom.on('connection', this._handleOperatorConnection.bind(this));
    }

    // Creates an object that stores a customer connection and has
    // the ability to delete itself when the customer disconnects
    _handleCustomerConnection(socket) {
        const onDisconnect = () => {
            delete this.customerConnections[socket.id];
        };
        this.customerConnections[socket.id] = new CustomerConnectionHandler(socket, this, onDisconnect);
    }

    // Same as above, but for operator connections
    _handleOperatorConnection(socket) {
        const onDisconnect = () => {
            delete this.customerConnections[socket.id];
        };
        this.operatorConnections[socket.id] = new OperatorConnectionHandler(socket, this, onDisconnect);
    }

    // Notifies all operators of a customer's connection changing - CONNECTED and DISCONNECTED
    _sendConnectionStatusToOperator(customerId, disconnected) {
        console.log('Sending customer id to any operators');
        const status = disconnected ?
            AppConstants.EVENT_CUSTOMER_DISCONNECTED :
            AppConstants.EVENT_CUSTOMER_CONNECTED;
        this.operatorRoom.emit(status, customerId);
        // We're using Socket.io for our chat, which provides a synchronous API. However, in case
        // you want to swich it out for an async call, this method returns a promise.
        return Promise.resolve();
    }

    _sendOperatorJoined(customerId) {
        console.log("BROADCASTING OPERATOR JOINED :" + customerId);
        this.operatorRoom.emit(AppConstants.EVENT_OPERATOR_JOINED_CUSTOMER, customerId);
        return Promise.resolve();
    }

    parseDialogflowResponse(responses, customerId) {
        const result = responses[0];
        var replyMessage = {
            messageType: "message",
            content: result.queryResult.fulfillmentText,
            from: "Agent",
            customerId: customerId,
            timestamp: new Date(),
            intent: result.queryResult.intent.displayName,
        };
        try {
            var richResponse = responses[0].queryResult.fulfillmentMessages[1].payload.fields.richContent.listValue.values[0].listValue.values[0].structValue.fields.options.listValue.values.map(e => e.structValue.fields.text.stringValue);
            if (richResponse) {
                replyMessage.richResponseType = 'chips';
                replyMessage.options = richResponse;
                this._addDefaultOptions(replyMessage);
            }
            console.log(richResponse);

        } catch {

        }

        return replyMessage;
    }

    _addDefaultOptions(replyMessage) {
        if (replyMessage.options)
            replyMessage.options.push(AppConstants.TALK_TO_HUMAN_OPTION);
    }

    // Given details of a customer and their utterance, decide what to do.
    _routeCustomer(message, customer, customerId) {

        // Since all customer messages should show up in the operator chat,
        // we now send this utterance to all operators
        return this._sendUtteranceToOperator(message, customer)
            .then(() => {
                // So all of our logs end up in Dialogflow (for use in training and history),
                // we'll always send the utterance to the agent - even if the customer is in operator mode.
                return this._sendUtteranceToAgent(message, customer);
            })
            .then(responses => {
                var replyMessage = this.parseDialogflowResponse(responses, message.customerId);
                // If the customer is in agent mode, we'll forward the agent's response to the customer.
                // If not, just discard the agent's response.
                if (customer.mode === CustomerStore.MODE_AGENT) {
                    this.messageStore._pushMessage(replyMessage);
                    // If the agent indicated that the customer should be switched to operator
                    // mode, do so
                    if (this._checkOperatorMode(replyMessage)) {
                        return this._switchToOperator(replyMessage.customerId, customer, replyMessage);
                    }
                    // If not in operator mode,
                    // Send the agent's response to the operator so they see both sides
                    // of the conversation.
                    this._sendUtteranceToOperator(replyMessage, customer, true);
                    // Return the agent's response so it can be sent to the customer down the chain
                    return replyMessage;
                }
            });
    }

    // Uses the Dialogflow client to send a 'WELCOME' event to the agent, starting the conversation.
    _sendEventToAgent(customer) {
        console.log('Sending WELCOME event to agent');
        return this.client.detectIntent({
            // Use the customer ID as Dialogflow's session ID
            session: this.client.sessionPath(this.projectId, customer.id),
            queryInput: {
                event: {
                    name: 'WELCOME',
                    languageCode: 'en'
                }
            }
        });
    }

    // Sends an utterance to Dialogflow and returns a promise with API response.
    _sendUtteranceToAgent(message, customer) {
        console.log('Sending utterance to agent');
        return this.client.detectIntent({
            // Use the customer ID as Dialogflow's session ID
            session: this.client.sessionPath(this.projectId, customer.id),
            queryInput: {
                text: {
                    text: message.content,
                    languageCode: 'en'
                }
            }
        });
    }

    // Send an utterance, or an array of utterances, to the operator channel so that
    // every operator receives it.
    _sendUtteranceToOperator(messages, customer, isAgentResponse) {
        console.log('Sending utterance to any operators');
        if (Array.isArray(messages)) {
            messages.forEach(message => {
                this.operatorRoom.emit(AppConstants.EVENT_CUSTOMER_MESSAGE, message);
            });
        } else {
            this.operatorRoom.emit(AppConstants.EVENT_CUSTOMER_MESSAGE, messages);
        }
        // We're using Socket.io for our chat, which provides a synchronous API. However, in case
        // you want to swich it out for an async call, this method returns a promise.
        return Promise.resolve();
    }

    // If one operator sends a message to a customer, share it with all connected operators
    _relayOperatorMessage(message) {
        this.operatorRoom.emit(AppConstants.EVENT_OPERATOR_MESSAGE, message);
        // We're using Socket.io for our chat, which provides a synchronous API. However, in case
        // you want to swich it out for an async call, this method returns a promise.
        return Promise.resolve();
    }

    // Factory method to create message objects in the format expected by the operator client
    _operatorMessageObject(customerId, utterance, isAgentResponse) {
        return {
            customerId: customerId,
            utterance: utterance,
            isAgentResponse: isAgentResponse || false
        };
    }

    // Examines the context from the Dialogflow response and returns a boolean
    // indicating whether the agent placed the customer in operator mode
    _checkOperatorMode(replyMessage) {
        let operatorMode = false;
        if (replyMessage.intent === AppConstants.INTENT_OPERATOR_REQUEST) {
            operatorMode = true;
        }
        return operatorMode;
    }

    // Place the customer in operator mode by updating the stored customer data,
    // and generate an introductory "human" response to send to the user.
    _switchToOperator(customerId, customer, response) {
        console.log('Switching customer to operator mode');
        customer.mode = CustomerStore.MODE_OPERATOR;
        return this.customerStore
            .setCustomer(customerId, customer)
            .then(this._notifyOperatorOfSwitch(customerId, customer))
            .then(() => {
                // We return an array of two responses: the last utterance from the Dialogflow agent,
                // and a mock "human" response introducing the operator.
                // Also send everything to the operator so they can see how the agent responded
                this._sendUtteranceToOperator(response, customer, true);
                return response;
            });
    }

    // Inform the operator channel that a customer has been switched to operator mode
    _notifyOperatorOfSwitch(customerId) {
        this.operatorRoom.emit(AppConstants.EVENT_CUSTOMER_REQUESTED_OPERATOR, customerId);
        // We're using Socket.io for our chat, which provides a synchronous API. However, in case
        // you want to swich it out for an async call, this method returns a promise.
        return Promise.resolve();
    }

}

module.exports = MessageRouter;